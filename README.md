# Docker for Windows Server

Notes and tips about how to setup and run Docker on Windows Server

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Index
## [Windows 2016](https://gitlab.com/dieffe/docker-for-windows-server/blob/master/README.md#windows-2016)
- ### [Windows Containers](https://gitlab.com/dieffe/docker-for-windows-server/blob/master/README.md#windows-containers)
- ### [Linux Containers](https://gitlab.com/dieffe/docker-for-windows-server/blob/master/README.md#linux-containers)

## [Windows 2019](https://gitlab.com/dieffe/docker-for-windows-server/blob/master/README.md#windows-2019)
- ### [Windows Containers](https://gitlab.com/dieffe/docker-for-windows-server/blob/master/README.md#windows-containers-1)
- ### [Linux Containers](https://gitlab.com/dieffe/docker-for-windows-server/blob/master/README.md#linux-containers-1)

## [Commands](https://gitlab.com/dieffe/docker-for-windows-server/blob/master/README.md#commands-1)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# Windows 2016

## Windows Containers

Install Containers 

```powershell
Install-WindowsFeature -Name containers -Restart
```

Install Docker EE
```powershell
Install-Module DockerMsftProvider -Force
Install-Package Docker -ProviderName DockerMsftProvider -Force
```
Test Windows container (Host OS Build 1607)
```
 docker run --rm -ti mcr.microsoft.com/windows/servercore:1607 cmd
```

## Linux Containers

### OS Build <=1607

OS Build 1709+ (kernel 10.0 16299 (16299.15.amd64fre.rs3_release.170928-1534) seems to be required to use Linux Containers.
Up to now there's no update to raise Windows Server 2016 from build 1607 to 1709 and it seems to ba a Server Core only build.

[Windows Server 1709: What’s New](https://www.silviodibenedetto.com/windows-server-1709-whats-new/)

> Do you have a virtual machine with IIS installed and the plan is upgrade to v1709? No way! Upgrade in-place is not possible and supported, this means that you must the OS from beginning. Don’t forget that the build is available only as Server Core so the management is via cmd or PowerShell.

### OS Build 1709+ (WIP - corrently not working)

*this setup can run both windows and linux containers*

Setup should be similar to Windows 2019, but dockerd seems unable to use LCOW




# Windows 2019

## Windows Containers
(WIP)


## Linux Containers
*this setup can run both windows and linux containers*

Install Hyper-V 
```powershell
Install-WindowsFeature -Name Hyper-V -IncludeManagementTools
```

Install Containers 

```powershell
Install-WindowsFeature -Name containers -Restart
```

Install Docker EE
```powershell
Install-Module DockerMsftProvider -Force
Install-Package Docker -ProviderName DockerMsftProvider -Force

```

Set default Docker service to manual
```powershell
Set-Service -Name "Docker" -StartupType Manual
```

Install LCOW package

> Download [Release.zip](https://github.com/linuxkit/lcow/releases/download/v4.14.35-v0.3.9/release.zip) and extract the content in C:\Program Files\Linux Containers

```powershell
New-Item -Path $Env:ProgramFiles -Name "Linux Containers" -ItemType "directory"
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Invoke-WebRequest -Uri 'https://github.com/linuxkit/lcow/releases/download/v4.14.35-v0.3.9/release.zip' -OutFile $Env:ProgramFiles'\Linux Containers\Release.zip'
Expand-Archive $Env:ProgramFiles'\Linux Containers\Release.zip' -DestinationPath $Env:ProgramFiles'\Linux Containers'
Remove-Item $Env:ProgramFiles'\Linux Containers\Release.zip'
```

Create a new Docker Experimental service
```powershell
New-Service -Name "Docker Experimental" -BinaryPathName '"C:\Program Files\Docker\dockerd.exe" -D --experimental --run-service' 
Set-Service -Name "Docker Experimental" -StartupType Automatic
Start-Service -name "Docker Experimental"
```

Test linux container
```
docker run --platform linux --rm -ti alpine sh
```

Test Windows container (*check your Windows Server build version using winver and run the image tag accordingly*)
```
docker run --rm -ti mcr.microsoft.com/windows/nanoserver:1809 cmd
```


# Commands

## Uninstall Docker EE

Leave any active Docker Swarm
```powershell
docker swarm leave --force
```

Remove all running and stopped containers
```powershell
docker rm -f $(docker ps --all --quiet)
```

Prune container data
```powershell
docker system prune --all --volumes
```

Uninstall Docker PowerShell Package and Module
```powershell
Uninstall-Package -Name docker -ProviderName DockerMsftProvider
Uninstall-Module -Name DockerMsftProvider
```


Remove Docker services
```powershell
Stop-Service -name "Docker"
Stop-Service -name "Docker Experimental"
Remove-Service -Name "Docker"
Remove-Service -Name "Docker Experimental"
```

Remove LCOW
```powershell
Remove-Item -Path $Env:ProgramFiles'\Linux Containers' -Force -Recurse

```

Remove Hyper-v and Containers
```powershell
Remove-WindowsFeature -Name Hyper-V -IncludeManagementTools
Remove-WindowsFeature -Name containers -IncludeManagementTools -restart
```

Clean up Windows Networking and file system
```powershell
Get-HNSNetwork | Remove-HNSNetwork
Remove-Item -Path $Env:ProgramFiles'\Docker' -Recurse -Force
Remove-Item -Path $Env:ProgramData'\docker' -Recurse -Force
Remove-Item -Path $HOME'\.docker' -Recurse -Force
```


## Info & Version

Display system-wide information
```
docker info
```

Show the Docker version information
```
docker version
```

## Images & Containers

Search Docker Hub for images
```
docker search <whatever>
```

Pull an image or a repository from a registry
```
docker pull <image>
```

List the running containers
```
docker ps
```

List all running and exited containers
```
docker ps -a
```

Lists all the locally stored docker images
```
docker images
```

Build an image from a specified docker file
```
docker build <path to docker file>
```

*BUILD Options*
*   -t tag an image (ie: docker build -t <username/image name:tag> <path to docker file> )


Delete a stopped container
```
docker rm <container id>
```

Delete all existing containers
```powershell
docker rm $(docker ps -a -q)
```


Delete an image from local storage
```
docker rmi <image-id>
```

Delete all existing images from local storage
```powershell
docker rmi $(docker images -q -a)
```

Start a container from an image
```
docker run <image name>
```

*RUN Options*

*  -it used for interactive processes like shells
*  -d launches a container in detach mode
*  -p exposes/nat tcp ports (ie -p 80:80 / -p host_port:container_port)

Access a running linux container
```
docker exec -it <container id> bash
```

Stop a running container
```
docker stop <container id>
```

Stop all running containers
```powershell
docker stop $(docker ps -a -q)
```

Stop a running container immediately
```
docker kill <container id>
```

Delete all unused containers, unused networks, and dangling images
```
docker system prune
```

## Repository/Registry interactions

Login to the docker hub repository/registry
```
docker login
```

Push an image to the docker hub repository
```
docker push <username/image name>
```